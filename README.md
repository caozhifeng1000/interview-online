# interview-online

#### 介绍
基于 koa、socket.io、webRTC、monaco-editor 实现的在线视频面试功能<br>
目前仅实现 js 相关功能，可进一步拓展语言支持，以满足不同开发岗的视频面试需要


#### 运行
在项目根目录打开命令行工具，然后执行<br>
npm install<br><br>

然后在 static 目录下打开命令行工具，然后执行<br>
npm install<br>
npm run build<br><br>

最后回到根目录打开命令行工具，然后执行<br>
npm run start<br><br>

打开浏览器，访问下面地址<br>
地址格式：http://127.0.0.1:3721/#/房间号/标识<br>
http://127.0.0.1:3721/#/001/0 （标识：0 - 面试官）<br>
http://127.0.0.1:3721/#/001/1 （标识：1 - 候选人）<br><br>