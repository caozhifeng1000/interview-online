module.exports = {
  root: true,
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  },
  env: {
    browser: true,
  },
  extends: ['standard', 'plugin:vue/base'],
  plugins: [
    'vue'
  ],
  rules: {
    'arrow-parens': 0,
    'generator-star-spacing': 0,
    'no-eval': 0,
    'no-tabs': 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    'semi': ['error', 'never'],
    'space-before-function-paren': ['error', 'always'],
    'comma-dangle': 0,
    'no-extra-semi': 0,
    'new-cap': 0,
    'no-new': 0,
    'indent': ['error', 2],
    'no-mixed-spaces-and-tabs': [2, false],
    'no-irregular-whitespace': 2,
    'no-invalid-this': 2,
    'no-multiple-empty-lines': [
      'error',
      {
        'max': 10,
        'maxBOF': 1
      }
    ],
    'quote-props': [
      'error',
      'as-needed',
      {
        'unnecessary': false
      }
    ]
  }
};
