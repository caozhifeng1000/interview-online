export default [
  {
    path: '/:roomId/:type',
    name: 'home',
    meta: {
      name: '主页',
    },
    component: () => import(/* webpackChunkName: "home" */ '@/views/home.vue')
  }
]
