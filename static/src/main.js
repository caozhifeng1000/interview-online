import '@/less/common.less'
import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from '@/routes/index'
import mainView from '@/views/main.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  routes
})

window.MonacoEnvironment = {
  getWorkerUrl: function (moduleId, label) {
    const map = {
      json: './json.worker.js',
      css: './css.worker.js',
      html: './html.worker.js',
      typescript: './ts.worker.js',
      javascript: './ts.worker.js',
      sql: './sql.worker.js'
    }

    return map[label] || './editor.worker.js'
  }
}

new Vue({
  el: '#app',
  router,
  render (h) {
    return h(mainView)
  }
})
